# Terraform VPC
This is a terraform project to create a vpc with public, private, and database subnets. It also adds the appropriate route table for each subnet.