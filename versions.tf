# Terraform Block
terraform {
  required_version = "~> 0.14"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  } 
}  
# Provider Block
provider "aws" {
  region = var.aws_region
  profile = "iamadmin2-prod"
}

/*
Note-1:  AWS Credentials Profile (profile = "iamadmin2-prod") configured on your local system  
$HOME/.aws/credentials
*/
